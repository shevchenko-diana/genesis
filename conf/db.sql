CREATE DATABASE shortener;

 CREATE TABLE counter(
   ID            INT PRIMARY KEY NOT NULL,
   COUNTER       INT NOT NULL
);

 CREATE TABLE links(
   ID            INT PRIMARY KEY NOT NULL,
   URl           CHAR(50) NOT NULL,
   HASH          CHAR(50) NOT NULL
);
