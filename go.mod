module genesis-url-shortener

go 1.13

require (
	github.com/gopherjs/gopherjs v0.0.0-20190430165422-3e4dfb77656c // indirect
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.7.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/smartystreets/assertions v1.0.1 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/withmandala/go-log v0.1.0
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
	gopkg.in/ini.v1 v1.46.0
)
