## URL shortener server

### Task 
Implement HTTP server that can generate shortened URLs - requests to shortened URLs must be either redirected, or return 404.

##### Optional
- URLs should be persisted locally to a storage (MySQL, PostgreSQL, Redis, CSV, etc)
- The redirect requests should be cached in memory for certain amount of time.

### Notes on Implementation
1. The config should be called **config.ini** - the example of such is included in conf/ folder. I have grown used to never include actual configs, and did not want to make exceptions to the habit.

2. The initialization of the server is based on the assumption that the database/tables are already created, since it is not the core problem of the task

3. Basically, shortener revolves around a simple operation of converting a link sequence number into base62.

### Routes
```
POST http://localhost:4000/create/
Content-Type: application/json

{
  "link": "https://www.google.com/search?q=genesis"
}
### 1 - is the shortened link hash 
GET http://localhost:4000/1
Content-Type: application/json
```

###
### what would you improve?

- first of all, the initialization process is not suitable for a larger application. I would probably solve this with the help of ["github.com/facebookgo/inject]()
- graceful exit - not perfect, also system channels should be listened to
- extract db logic further into an interface to simplify change of the db type if needed

