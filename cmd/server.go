package main

import (
	"context"
	"genesis-url-shortener/pkg/api"
	"genesis-url-shortener/pkg/log"
	"genesis-url-shortener/pkg/settings"
	"golang.org/x/sync/errgroup"
	"gopkg.in/ini.v1"
	"time"
)

var confPath = "conf/conf.ini"

type Server struct {
	context            context.Context
	shutdownFn         context.CancelFunc
	childRoutines      *errgroup.Group
	cfg                *settings.Config
	shutdownReason     string
	shutdownInProgress bool

	HttpServer *api.HttpServer
	log        *log.Logger
}

func NewServer() *Server {
	rootCtx, shutdownFn := context.WithCancel(context.Background())
	childRoutines, childCtx := errgroup.WithContext(rootCtx)

	return &Server{
		context:       childCtx,
		shutdownFn:    shutdownFn,
		childRoutines: childRoutines,
		log:           log.NewLogger(),
		cfg:           &settings.Config{},
		HttpServer:    api.NewHttpServer(),
	}
}
func (s *Server) Init() error {
	err := s.HttpServer.Init(s.cfg)
	return err
}

func (s *Server) initializeService(ctx context.Context) {

}

type BackgroundService interface {
	Run(ctx context.Context) error
}

func (s *Server) Run(ctx context.Context) (err error) {
	services := []BackgroundService{s.HttpServer, s.HttpServer.Db}

	for _, service := range services {
		//service is a single variable, need this to avoid init of the last service multiple times
		srv := service
		s.childRoutines.Go(func() error {
			if s.shutdownInProgress {
				return nil
			}

			err := srv.Run(s.context)
			s.shutdownInProgress = true

			return err
		})
	}
	defer func() {
		s.log.Info("Waiting on services...")
		if waitErr := s.childRoutines.Wait(); waitErr != nil {
			s.log.Error(waitErr.Error())
			if err == nil {
				err = waitErr
			}
		}

	}()

	return
}

func (s *Server) loadConfig() error {

	confFile, err := ini.Load(confPath)
	if err != nil {
		s.log.Error("config file not found at " + confPath)
		return err
	}

	httpSect := confFile.Section("http")
	s.cfg.Address = httpSect.Key("addr").MustString("localhost")
	s.cfg.Port = httpSect.Key("port").MustString("3000")
	s.cfg.Protocol = httpSect.Key("protocol").MustString("http")

	dbSect := confFile.Section("database")
	s.cfg.DatabaseConf.Host = dbSect.Key("host").MustString("localhost")
	s.cfg.DatabaseConf.Port = dbSect.Key("port").MustString("5432")
	s.cfg.DatabaseConf.User = dbSect.Key("user").MustString("postgres")
	s.cfg.DatabaseConf.Password = dbSect.Key("password").MustString("postgres")
	s.cfg.DatabaseConf.Name = dbSect.Key("name").MustString("shortener")

	cache := confFile.Section("cache")
	s.cfg.CacheExpiration = cache.Key("expiration").MustDuration(time.Hour)

	return nil
}
