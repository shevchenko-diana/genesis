package main

import (
	"fmt"
	"os"
)

func main() {
	server := NewServer()
	server.log.Info("initializing server...")

	if err := server.loadConfig(); err != nil {
		server.log.Error("initialization failed, exiting...")
		os.Exit(1)
	}

	if err := server.Init(); err != nil {
		server.log.Error("server init failed")
		os.Exit(1)
	}

	if err := server.Run(server.context); err != nil {
		server.log.Error(fmt.Sprintf("Server shutdown, reason %s", err.Error()))
	}

}
