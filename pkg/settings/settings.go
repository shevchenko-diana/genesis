package settings

import "time"

type Config struct {
	//http
	Address  string
	Port     string
	Protocol string

	//database
	DatabaseConf DatabaseConf

	//cache
	CacheExpiration time.Duration
}

type DatabaseConf struct {
	Host     string
	Port     string
	User     string
	Password string
	Name     string
}
