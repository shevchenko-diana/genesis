package models

import "errors"

var ErrLinkNotFound = errors.New("link was not found")

type Link struct {
	Id        int64
	Url       string
	ShortHash string
}

type Counter struct {
	Id    int64
	Count int64
}
