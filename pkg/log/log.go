package log

import (
	"fmt"
	"github.com/withmandala/go-log/colorful"
	"log"
	"os"
)

type Logger struct {
	log *log.Logger
}

type colorFunc func(data []byte) []byte

func NewLogger() *Logger {
	return &Logger{log: log.New(os.Stdout, "url-shortener: ", log.Ltime)}
}

func (l Logger) Error(msg interface{}) {
	l.logMsg("error :", msg, colorful.Red)
}

func (l Logger) Info(msg interface{}) {
	l.logMsg("info :", msg, colorful.Blue)
}

func (l Logger) logMsg(prefix string, msg interface{}, color colorFunc) {
	l.log.Println(colorize(fmt.Sprint(prefix, msg), color))
}

func colorize(msg string, color colorFunc) string {
	return string(color([]byte(msg)))
}
