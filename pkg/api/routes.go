package api

import (
	"encoding/json"
	"fmt"
	"genesis-url-shortener/pkg/models"
	"github.com/gorilla/mux"
	"github.com/patrickmn/go-cache"
	"net/http"
	"net/url"
)

type Body struct {
	Link string `json:"link"`
}

func (srv *HttpServer) createNewLink(response http.ResponseWriter, req *http.Request) {

	var reqBodyStr = Body{}
	//reqBody, err := ioutil.ReadAll(req.Body)
	//if err != nil {
	//	response.WriteHeader(http.StatusUnprocessableEntity)
	//	srv.log.Error("Could not process request, err" + err.Error())
	//	return
	//}
	if err := json.NewDecoder(req.Body).Decode(&reqBodyStr); err != nil {
		response.WriteHeader(http.StatusUnprocessableEntity)
		srv.log.Error("Could not process request, err" + err.Error())
		return
	}

	if !isValid(reqBodyStr.Link) {
		response.WriteHeader(http.StatusUnprocessableEntity)
		return
	}
	newLink, err := srv.Db.CreateNewLink(reqBodyStr.Link)
	if err != nil {
		srv.log.Error(err.Error())
		response.WriteHeader(http.StatusInternalServerError)
		return
	}
	resp, err := json.Marshal(Body{Link: srv.formatLink(newLink)})
	if err != nil {
		srv.log.Error(err.Error())
		response.WriteHeader(http.StatusInternalServerError)
		return
	}

	if _, err = response.Write(resp); err != nil {
		srv.log.Error(err.Error())
	}

}

func (srv *HttpServer) getShortLink(resp http.ResponseWriter, req *http.Request) {

	shortLink, ok := mux.Vars(req)["shortLink"]
	if !ok {
		resp.WriteHeader(http.StatusUnprocessableEntity)
		return
	}
	linkFound, found := srv.cache.Get(shortLink)
	if found {
		link := linkFound.(string)
		http.Redirect(resp, req, link, http.StatusFound)
		return
	}

	originalLink, err := srv.Db.GetShortLink(shortLink)
	if err != nil {
		if err == models.ErrLinkNotFound {
			resp.WriteHeader(http.StatusNotFound)
		} else {
			resp.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	srv.cache.Set(shortLink, originalLink, cache.DefaultExpiration)

	http.Redirect(resp, req, originalLink, http.StatusFound)

}

func (srv *HttpServer) formatLink(link string) string {
	return fmt.Sprintf("%s://%s:%s/%s", srv.cfg.Protocol, srv.cfg.Address, srv.cfg.Port, link)
}

func isValid(link string) bool {
	u, err := url.Parse(link)
	return err == nil && u.Scheme != "" && u.Host != ""
}
