package api

import (
	"context"
	"fmt"
	"genesis-url-shortener/pkg/database"
	"genesis-url-shortener/pkg/log"
	"genesis-url-shortener/pkg/settings"
	"github.com/gorilla/mux"
	"github.com/patrickmn/go-cache"
	"github.com/pkg/errors"
	"net"
	"net/http"
)

type HttpServer struct {
	Db *database.Db

	cfg     *settings.Config
	log     *log.Logger
	httpSrv *http.Server
	cache   *cache.Cache
}

func NewHttpServer() *HttpServer {
	return &HttpServer{
		Db:  &database.Db{},
		log: log.NewLogger(),
	}
}

func (srv *HttpServer) Init(cfg *settings.Config) error {
	srv.cfg = cfg
	srv.cache = cache.New(srv.cfg.CacheExpiration, srv.cfg.CacheExpiration)

	if err := srv.Db.Init(cfg); err != nil {
		srv.log.Error("http-server init failed: " + err.Error())
		return err
	}
	return nil
}

func (srv *HttpServer) Run(ctx context.Context) error {

	srv.httpSrv = &http.Server{
		Addr:    fmt.Sprintf("%s:%s", srv.cfg.Address, srv.cfg.Port),
		Handler: srv.routes(),
	}
	srv.log.Info(fmt.Sprintf("HTTP Server Listen: address %s, port %s", srv.cfg.Address, srv.cfg.Port))

	listener, err := net.Listen("tcp", srv.httpSrv.Addr)
	if err != nil {
		return errors.New("failed to open listener on address " + srv.httpSrv.Addr)
	}
	if err := srv.httpSrv.Serve(listener); err != nil {
		if err == http.ErrServerClosed {
			srv.log.Info("server was shutdown gracefully")
			return nil
		}
		return err
	}
	return nil
}

func (srv *HttpServer) routes() *mux.Router {
	m := mux.NewRouter()
	m.HandleFunc("/{shortLink}", srv.getShortLink).Methods("GET")
	m.HandleFunc("/create/", srv.createNewLink).Methods("POST")

	return m
}
