package database

type DbInterface interface {
	CreateNewLink(url string) (string, error)
	GetShortLink(hash string) (string, error)
}
