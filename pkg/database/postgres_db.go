package database

import (
	"context"
	"fmt"
	"genesis-url-shortener/pkg/models"
	"genesis-url-shortener/pkg/settings"
	"github.com/pkg/errors"
	"math/big"

	"database/sql"

	_ "github.com/lib/pq"
)

type Db struct {
	cfg *settings.Config
	db  *sql.DB
}

func (d *Db) Init(cfg *settings.Config) error {
	d.cfg = cfg
	return nil
}

func (d *Db) Run(ctx context.Context) error {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		d.cfg.DatabaseConf.Host, d.cfg.DatabaseConf.Port, d.cfg.DatabaseConf.User, d.cfg.DatabaseConf.Password, d.cfg.DatabaseConf.Name)
	var err error
	d.db, err = sql.Open("postgres", psqlInfo)

	if err != nil {
		return errors.Wrap(err, "Unable to connect to database")
	}
	for {
		select {
		case <-ctx.Done():
			return d.db.Close()
		}
	}

}

func (d *Db) CreateNewLink(url string) (string, error) {
	counter, err := d.incrementLinkCounter()
	if err != nil {
		return "", errors.Wrap(err, "could not create new link")
	}

	newLinkHash := big.NewInt(counter).Text(62)
	err = d.saveNewLink(models.Link{
		Id:        counter,
		Url:       url,
		ShortHash: newLinkHash,
	})

	return newLinkHash, nil
}

func (d *Db) GetShortLink(hash string) (string, error) {
	var originalUrl string
	_ = d.db.QueryRow(`Select URL from LINKS Where HASH = $1`, hash).Scan(&originalUrl)

	if originalUrl == "" {
		return "", models.ErrLinkNotFound
	}

	return originalUrl, nil
}

func (d *Db) saveNewLink(link models.Link) error {
	_, err := d.db.Exec(`INSERT INTO LINKS (ID, URL, HASH) VALUES ($1,$2,$3);`,
		link.Id, link.Url, link.ShortHash)
	return err

}

func (d *Db) incrementLinkCounter() (int64, error) {
	var counter models.Counter
	errCtx := "db request failed"
	err := d.db.QueryRow(`SELECT id, counter from COUNTER;`).Scan(&counter.Id, &counter.Count)
	if err == sql.ErrNoRows {
		if _, err := d.db.Exec(`INSERT INTO COUNTER (ID, COUNTER) VALUES (1, 1);`); err != nil {
			return 0, errors.Wrap(err, errCtx)
		}
	} else {
		return 0, errors.Wrap(err, errCtx)
	}
	counter.Count++
	if _, err = d.db.Exec(`UPDATE COUNTER SET COUNTER = $1`, counter.Count); err != nil {
		return 0, errors.Wrap(err, errCtx)
	}

	return counter.Count, nil
}
